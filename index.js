'use strict';

const express = require('express');
const app = express();
const bodyParser = require('body-parser');
const cors = require('cors');
const fs = require('fs');
const util = require('util');
const moment = require('moment');
const port = 9999;
const readFile = util.promisify(fs.readFile);
const PharmacyStateNotification = [
  'UNPROCEED_PRESCRIPTION',
  'CUSTOMER_PAYMENT_EXPIRED',
  'CUSTOMER_PAYMENT_SUCCESS',
  'CUSTOMER_REFUND_PROCESSED',
  'ORDER_DECLINED_BY_PHARMACY',
  'ORDER_IS_PROCESSED_BY_PHARMACY',
  'ORDER_CANCELED_BY_PHARMACY',
  'ORDER_CANCELED_BY_COURIER',
  'ORDER_IS_PICKED_BY_COURIER',
  'ITEM_ON_THE_WAY_TO_CUSTOMER',
  'ITEM_DELIVERED',
];

app.use(cors());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

app.get('/', (req, res) => {
  res.send('Hello World, from express');
});

app.post('/webhook', (req, res) => {
  const fileName = moment().utc().utcOffset('+07:00').format('YYYY-MM-DD');
  if (PharmacyStateNotification.includes(req.body.type || req.body.key )) {
    const newData = JSON.stringify(req.body);
    fs.appendFile(`./data/${fileName}.txt`, newData + "\n", function (err) {
      if (err) throw err;
    });
  }
  res.json({
    status: true,
    message: 'Success'
  });
  res.status(201);
});

app.get('/result', (req, res) => {
  const fileName = moment().utc().utcOffset('+07:00').format('YYYY-MM-DD');
  const param = req.query.date ? `${req.query.date }.txt` : `${fileName}.txt`
  try {
    const fileData = fs.readFileSync(`./data/${param}`, 'utf8')
    const result = fileData.split('\n')
    let data = [];
    result.forEach((_, i) => {
      if(result.length === i+1) return;
      const jsonData = JSON.parse(result[i])
      data.push({
        ...jsonData,
        dateFormat: moment.unix(jsonData.timestamp)
                    .utc()
                    .utcOffset('+07:00')
                    .format('YYYY-MM-DD HH:mm:ss'),
      });
    })
    res.json(data);
    res.status(200);
  } catch (err) {
    res.json({
      status: false,
      message: 'No record'
    });
    res.status(404);
  }
});

app.listen(port, () => console.log(`Hello world app listening on port ${port}!`))

function getStuff() {
  return readFile('test');
}
